# Devops-Challenge-2 #

This project will create EKS cluster in AWS with the following requirements using Terraform

* Creating a VPC in AWS with 2 public and 2 private subnets.
* The EKS cluster in AWS will use the VPC created in the previous step.
* The cluster must have 2 nodes, using instance type `t3a.large`.
* The nodes must be on the private subnets only.

## Start Here ###

The Amazon Elastic Kubernetes Service (EKS) is the AWS service for deploying, managing, and scaling containerized applications with Kubernetes.

## Prerequisites ###

If you're new to Terraform itself, refer first to the Getting Started [tutorial](https://learn.hashicorp.com/collections/terraform/aws-get-started).

For this installation, you will need:

* an [AWS account](https://portal.aws.amazon.com/billing/signup?nc2=h_ct&src=default&redirect_url=https%3A%2F%2Faws.amazon.com%2Fregistration-confirmation#/start) with the IAM permissions listed on the [EKS module documentation](https://github.com/terraform-aws-modules/terraform-aws-eks/blob/master/docs/iam-permissions.md),
* a configured AWS CLI
* AWS IAM Authenticator
* [kubectl](https://learn.hashicorp.com/tutorials/terraform/eks#kubectl)

### Install AWS CLI

To install the AWS CLI, follow [these instructions](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html) or choose a package manager based on your operating system

After you've installed the AWS CLI, configure it by running `aws configure`

When prompted, enter your AWS Access Key ID, Secret Access Key, region and output format.


```
$ aws configure
AWS Access Key ID [None]: YOUR_AWS_ACCESS_KEY_ID
AWS Secret Access Key [None]: YOUR_AWS_SECRET_ACCESS_KEY
Default region name [None]: YOUR_AWS_REGION
Default output format [None]: json
```

If you don't have an AWS Access Credentials, create your AWS Access Key ID and Secret Access Key by navigating to your [service credentials](https://console.aws.amazon.com/iam/home?#/security_credentials) in the IAM service on AWS. Click "Create access key" here and download the file. This file contains your access credentials.

Your default region can be found in the AWS Web Management Console beside your username. Select the region drop down to find the region name (eg. us-east-1) corresponding with your location.

### Set up and initialize your Terraform workspace

In your terminal, clone this repository.

```
git clone https://bayramkaragoz@bitbucket.org/monites/devops-challenge-2.git
cd devops-challenge-2
```

### Initialize Terraform workspace

```
$ terraform init
$ terraform apply
```

Confirm the apply with a `yes`

Upon successful application, your terminal prints the outputs defined in `outputs.tf`

### Configure kubectl

Now that you've provisioned your EKS cluster, you need to configure kubectl

Run the following command to retrieve the access credentials for your cluster and automatically configure kubectl.

`$ aws eks --region $(terraform output -raw region) update-kubeconfig --name $(terraform output -raw cluster_name)`

The Kubernetes cluster name and region correspond to the output variables showed after the successful Terraform run.

### Next steps

After you update the kubeconfig for running kubectl command you can access to EKS cluster and deploy your application.
For more information please refer to [this tutorial](https://learn.hashicorp.com/tutorials/terraform/eks#set-up-and-initialize-your-terraform-workspace)
